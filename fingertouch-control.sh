#!/bin/bash

# Control of touchscreen finger

# input ID of the touchscreen as reported by `xinput list`
finger=$(xinput list | awk -F$'\t' '/Finger touch/ { split($2,a,"="); print a[2]; }')

if [[ ! $finger =~ ^[[:digit:]]+$ ]]; then
    notify-send -i error "Touchscreen Finger Control" "Could not determine xinput id for finger touch."
    exit 1
fi

if [ $1 ]; then
    [[ $1 =~ ^[01]$ ]] || { echo >&2 Invalid argument; exit 1; }
    control=$1
else
    state=$(xinput list-props $finger | awk -F: '/Device Enabled/ { print $2 }')
    if [ -z $state ]; then
        echo >&2 Error getting device info.
        exit 1
    fi
    let control=1-state
fi

xinput set-prop $finger "Device Enabled" $control
[ $control -eq 1 ] && control_text="activated" || control_text="deactivated"
notify-send -i input-tablet "Touchscreen Finger Control" "Finger control is $control_text"
